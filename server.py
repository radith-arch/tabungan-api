from dotenv import load_dotenv
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import uvicorn
import os
import argparse
import routes.registrasi as registrasi_api
import routes.update as update_api
import routes.getdata as getdata_api

PORT = int(os.getenv("PORT")) if os.getenv("PORT") is not None else 8000

if os.path.isfile('.env'):
	load_dotenv()


app = FastAPI()
origins = ["*"]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

#router fastapi
app.include_router(registrasi_api.router,prefix='')
app.include_router(update_api.router,prefix='')
app.include_router(getdata_api.router,prefix='')


@app.exception_handler(400)
async def bad_request(e):
    return {
		'success': False,
		'message': 'Terjadi masalah ketika proses data',
		'data': None
	}, 400

@app.exception_handler(404)
async def page_not_found(e):
    return {
		'success': False,
		'message': 'Endpoint path tidak ditemukan',
		'data': None
	}, 404

@app.exception_handler(500)
async def internal_error(e):
    return {
		'success': False,
		'message': 'Terjadi masalah ketika proses data',
		'data': None
	}, 500

if(__name__ == "__main__"):
        
	ap = argparse.ArgumentParser()  # argument parser

	uvicorn.run(app, port=PORT)
