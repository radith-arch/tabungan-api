from fastapi import APIRouter, Body, responses
from dotenv import load_dotenv
from dotenv import load_dotenv
import os
import json
from datetime import datetime
# import utils.helper as help

if os.path.isfile(".env"):
		load_dotenv()
	
DEBUG = os.getenv("DEBUG").lower() == "true" if os.getenv("DEBUG") is not None else False
DATABASE_JSON = os.getenv("DATABASE_JSON").lower() == "true" if os.getenv("DATABASE_JSON") is not None else False
DB_JSON_PATH = "/database_json" if os.getenv("DB_JSON_PATH") is None else os.getenv("DB_JSON_PATH")
KODE_BANK = os.getenv("KODE_BANK") if os.getenv("KODE_BANK") is not None else 123


if DATABASE_JSON:
	if not os.path.isdir(DB_JSON_PATH):
		os.mkdir(DB_JSON_PATH)

router = APIRouter()

@router.put("/tabung")
async def tabungFunc(tabung_req: dict = Body(...)):

	tabung_reqNorek = tabung_req["no_rekening"] if "no_rekening" in tabung_req else None
	tabung_reqNominal = tabung_req["nominal"] if "nominal" in tabung_req else None

	if tabung_reqNorek is None or tabung_reqNominal is None:

		detail_msg = {
				"success"   : False,
				"remark"   : "Saldo gagal ditambahkan, data tidak boleh kosong atau null!",
				"data": None
			}
		return responses.JSONResponse(content=detail_msg, status_code=400)

	if isinstance(tabung_reqNorek, str) is False or isinstance(tabung_reqNominal, int) is False :
		detail_msg = {
				"success"   : False,
				"remark"   : "Saldo gagal ditambahkan, no_rekening harus berbentuk string dan nominal harus berbentuk integer!",
				"data": None
			}
		return responses.JSONResponse(content=detail_msg, status_code=400)

	if tabung_reqNominal <= 0:
		detail_msg = {
				"success"   : False,
				"remark"   : "Saldo gagal ditambahkan, nominal tidak boleh sama atau lebih kecil dari nol!",
				"data": None
			}
		return responses.JSONResponse(content=detail_msg, status_code=400)


	check_num = [x for x in tabung_reqNorek if x.isalpha() or not x.isalnum() ]
	not_num = "".join(x for x in check_num)	

	if len(not_num) > 0:
		detail_msg = {
				"success"   : False,
				"remark"   : "Saldo gagal ditambahkan, no_rekening tidak valid!",
				"data": None
			}
		return responses.JSONResponse(content=detail_msg, status_code=400)

	pathFile = f"{DB_JSON_PATH}/{tabung_reqNorek}.json"
	if not os.path.isfile(pathFile):
		detail_msg = {
				"success"   : False,
				"remark"   : "Nomor rekening tidak dikenali!",
				"data": None
			}
		return responses.JSONResponse(content=detail_msg, status_code=400)


	if not DATABASE_JSON:
		detail_msg = {
				"success"   : False,
				"remark"   : "Mohon database diset sebagai JSON!",
				"data": None
			}
		return responses.JSONResponse(content=detail_msg, status_code=500)

	#sementara masih menggunakan database json
	if DATABASE_JSON:
		
		with open(pathFile) as f:
			dataNasabah = json.load(f)

		dataNasabah["nominal"] += tabung_reqNominal
		mutasi = {
            "waktu"             : datetime.now().strftime("%Y/%m/%d %H:%M:%S"),
            "kode_transaksi"    : "C", #untuk menabung | "D" #untuk tarik,
            "nominal"           : tabung_reqNominal

        }
		dataNasabah["mutasi"].append(mutasi)
		
		with open(pathFile, "w") as f:
			json.dump(dataNasabah, f)

		result = {
					"success"   : True,
					"remark"   : "Saldo berhasil ditambahkan!",
					"data": {
								"saldo" : dataNasabah["nominal"]
							}
				}
		
		return result

@router.put("/tarik")
async def tarikFunc(tarik_req: dict = Body(...)):

	tarik_reqNorek = tarik_req["no_rekening"] if "no_rekening" in tarik_req else None
	tarik_reqNominal = tarik_req["nominal"] if "nominal" in tarik_req else None
	
	if tarik_reqNorek is None or tarik_reqNominal is None:

		detail_msg = {
				"success"   : False,
				"remark"   : "Saldo gagal dikurang, data tidak boleh kosong atau null!",
				"data": None
			}
		return responses.JSONResponse(content=detail_msg, status_code=400)


	if isinstance(tarik_reqNorek, str) is False  or isinstance(tarik_reqNominal, int) is False :
		detail_msg = {
				"success"   : False,
				"remark"   : "Saldo gagal dikurang, no_rekening harus berbentuk string dan nominal harus berbentuk integer!",
				"data": None
			}
		return responses.JSONResponse(content=detail_msg, status_code=400)


	if tarik_reqNominal <= 0:
		detail_msg = {
				"success"   : False,
				"remark"   : "Saldo gagal dikurang, nominal tidak boleh sama atau lebih kecil dari nol!",
				"data": None
			}
		return responses.JSONResponse(content=detail_msg, status_code=400)


	check_num = [x for x in tarik_reqNorek if x.isalpha() or not x.isalnum() ]
	not_num = "".join(x for x in check_num)	

	if len(not_num) > 0:
		detail_msg = {
				"success"   : False,
				"remark"   : "Saldo gagal dikurang, no_rekening tidak valid!",
				"data": None
			}
		return responses.JSONResponse(content=detail_msg, status_code=400)


	pathFile = f"{DB_JSON_PATH}/{tarik_reqNorek}.json"
	if not os.path.isfile(pathFile):

		detail_msg = {
				"success"   : False,
				"remark"   : "Nomor rekening tidak dikenali!",
				"data": None
			}
		return responses.JSONResponse(content=detail_msg, status_code=400)

	if not DATABASE_JSON:
		detail_msg = {
				"success"   : False,
				"remark"   : "Mohon database diset sebagai JSON!",
				"data": None
			}
		return responses.JSONResponse(content=detail_msg, status_code=500)

	#sementara masih menggunakan database json
	if DATABASE_JSON:
		
		with open(pathFile) as f:
			dataNasabah = json.load(f)

		dataNasabah["nominal"] -= tarik_reqNominal

		if dataNasabah["nominal"] < 0:
			detail_msg = {
					"success"   : False,
					"remark"   : "Saldo tidak cukup!",
					"data": None
				}
			return responses.JSONResponse(content=detail_msg, status_code=400)

		mutasi = {
            "waktu"             : datetime.now().strftime("%Y/%m/%d %H:%M:%S"),
            "kode_transaksi"    : "D", #untuk tarik | "C" untuk menabung 
            "nominal"           : tarik_reqNominal

        }
		dataNasabah["mutasi"].append(mutasi)
		
		with open(pathFile, "w") as f:
			json.dump(dataNasabah, f)

		result = {
					"success"   : True,
					"remark"   : "Saldo berhasil dikurangi!",
					"data": {
								"saldo" : dataNasabah["nominal"]
							}
				}
		
		return result
