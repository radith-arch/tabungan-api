from fastapi import APIRouter, Body, responses
from dotenv import load_dotenv
from dotenv import load_dotenv
import os
import json
from datetime import datetime
import utils.helper as help

if os.path.isfile(".env"):
		load_dotenv()
	
DEBUG = os.getenv("DEBUG").lower() == "true" if os.getenv("DEBUG") is not None else False
DATABASE_JSON = os.getenv("DATABASE_JSON").lower() == "true" if os.getenv("DATABASE_JSON") is not None else False
DB_JSON_PATH = "/database_json" if os.getenv("DB_JSON_PATH") is None else os.getenv("DB_JSON_PATH")
KODE_BANK = os.getenv("KODE_BANK") if os.getenv("KODE_BANK") is not None else 123


if DATABASE_JSON:
	if not os.path.isdir(DB_JSON_PATH):
		os.mkdir(DB_JSON_PATH)

router = APIRouter()

@router.get("/saldo/{no_rekening}")
async def saldoFunc(no_rekening):

	check_num = [x for x in no_rekening if x.isalpha() or not x.isalnum() ]
	not_num = "".join(x for x in check_num)	

	if len(not_num) > 0:
		detail_msg = {
				"success"   : False,
				"remark"   : "Saldo gagal didapatkan, no_rekening tidak valid!",
				"data": None
			}
		return responses.JSONResponse(content=detail_msg, status_code=400)

	pathFile = f"{DB_JSON_PATH}/{no_rekening}.json"
	if not os.path.isfile(pathFile):
		detail_msg = {
				"success"   : False,
				"remark"   : "Nomor rekening tidak dikenali!",
				"data": None
			}
		return responses.JSONResponse(content=detail_msg, status_code=400)

	if not DATABASE_JSON:
		detail_msg = {
				"success"   : False,
				"remark"   : "Mohon database diset sebagai JSON!",
				"data": None
			}
		return responses.JSONResponse(content=detail_msg, status_code=500)

	#sementara masih menggunakan database json
	if DATABASE_JSON:
		
		with open(pathFile) as f:
			dataNasabah = json.load(f)

	result = {
				"success"   : True,
				"remark"   : "Saldo berhasil didapatkan!",
				"data": {
							"saldo" : dataNasabah["nominal"]
						}
			}
	
	return result

@router.get("/mutasi/{no_rekening}")
async def mutasiFunc(no_rekening):

	check_num = [x for x in no_rekening if x.isalpha() or not x.isalnum() ]
	not_num = "".join(x for x in check_num)	

	if len(not_num) > 0:
		detail_msg = {
				"success"   : False,
				"remark"   : "Mutasi gagal didapatkan, no_rekening tidak valid!",
				"data": None
			}
		return responses.JSONResponse(content=detail_msg, status_code=400)

	pathFile = f"{DB_JSON_PATH}/{no_rekening}.json"
	if not os.path.isfile(pathFile):
		detail_msg = {
				"success"   : False,
				"remark"   : "Nomor rekening tidak dikenali!",
				"data": None
			}
		return responses.JSONResponse(content=detail_msg, status_code=400)


	if not DATABASE_JSON:
		detail_msg = {
				"success"   : False,
				"remark"   : "Mohon database diset sebagai JSON!",
				"data": None
			}
		return responses.JSONResponse(content=detail_msg, status_code=500)

	#sementara masih menggunakan database json
	if DATABASE_JSON:
		
		with open(pathFile) as f:
			dataNasabah = json.load(f)

	result = {
				"success"   : True,
				"remark"   : "Mutasi rekening berhasil didapatkan!",
				"data": {
							"mutasi" : dataNasabah["mutasi"]
						}
			}
	
	return result
