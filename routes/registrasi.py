from fastapi import APIRouter, Body, responses
from dotenv import load_dotenv
from dotenv import load_dotenv
import os
import json
# from datetime import datetime
import utils.helper as help

if os.path.isfile(".env"):
		load_dotenv()
	
DEBUG = os.getenv("DEBUG").lower() == "true" if os.getenv("DEBUG") is not None else False
DATABASE_JSON = os.getenv("DATABASE_JSON").lower() == "true" if os.getenv("DATABASE_JSON") is not None else False
DB_JSON_PATH = "/database_json" if os.getenv("DB_JSON_PATH") is None else os.getenv("DB_JSON_PATH")
KODE_BANK = os.getenv("KODE_BANK") if os.getenv("KODE_BANK") is not None else 123


if DATABASE_JSON:
	if not os.path.isdir(DB_JSON_PATH):
		os.mkdir(DB_JSON_PATH)

router = APIRouter()

@router.post("/daftar")
async def daftarFunc(daftar_req: dict = Body(...)):

	daftar_reqNama = daftar_req["nama"] if "nama" in daftar_req else None
	daftar_reqNik = daftar_req["nik"] if "nik" in daftar_req else None
	daftar_reqNo_hp = daftar_req["no_hp"] if "no_hp" in daftar_req else None
	
	if daftar_reqNama is None or daftar_reqNik is None or daftar_reqNo_hp is None:

		detail_msg = {
				"success"   : False,
				"remark"   : "Tidak berhasil terdaftar, data tidak boleh kosong atau null!",
				"data": None
			}
		return responses.JSONResponse(content=detail_msg, status_code=400)

	if  isinstance(daftar_reqNama, str) is False or  isinstance(daftar_reqNik, str) is False or isinstance(daftar_reqNo_hp, str) is False :
		detail_msg = {
				"success"   : False,
				"remark"   : "Tidak berhasil terdaftar, nama, nik, dan no_hp harus berbentuk string!",
				"data": None
			}
		return responses.JSONResponse(content=detail_msg, status_code=400)


	check_num = [x for x in daftar_reqNik if x.isalpha() or not x.isalnum()]
	not_num = "".join(x for x in check_num)	
	if len(not_num) > 0:
		detail_msg = {
				"success"   : False,
				"remark"   : "Tidak berhasil terdaftar, nik harus berisikan angka dalam bentuk string!",
				"data": None
			}
		return responses.JSONResponse(content=detail_msg, status_code=400)

	check_nama = help.remove_all_whitespaces(daftar_reqNama)
	check_alpha = [x for x in check_nama if x.isdigit() or not x.isalnum() ]
	not_alpha = "".join(x for x in check_alpha)	

	if len(not_alpha) > 0:
		detail_msg = {
				"success"   : False,
				"remark"   : "Tidak berhasil terdaftar, nama tidak valid!",
				"data": None
			}
		return responses.JSONResponse(content=detail_msg, status_code=400)

	#untuk pengecekan nomor telepon valid atau tidak memerlukan waktu tamabah untuk explore
	#salah satu cara yg bisa dilakukan adalah dengan memberikan kode otp, namun cara ini tidak
	#bisa digunakan mengingat requirement yg diberikan
	if daftar_reqNo_hp[0] == "+":
		check_num = [x for x in daftar_reqNo_hp[1:] if x.isalpha() or not x.isalnum()]
	else:
		check_num = [x for x in daftar_reqNo_hp if x.isalpha() or not x.isalnum()]

	not_num = "".join(x for x in check_num)	
	if len(not_num) > 0:
		detail_msg = {
				"success"   : False,
				"remark"   : "Tidak berhasil terdaftar, no_hp harus berisikan angka dengan kode area +62 atau lain dan berbentuk string!",
				"data": None
			}
		return responses.JSONResponse(content=detail_msg, status_code=400)

	if "+62" not in daftar_reqNo_hp:
		if "0" in daftar_reqNo_hp[:3]:
			daftar_reqNo_hp = daftar_reqNo_hp.replace("0", "+62", 1)

		else:
			daftar_reqNo_hp = f"+62{daftar_reqNo_hp}"

	if not DATABASE_JSON:
		detail_msg = {
				"success"   : False,
				"remark"   : "Mohon database diset sebagai JSON!",
				"data": None
			}
		return responses.JSONResponse(content=detail_msg, status_code=500)

	#sementara masih menggunakan database json
	if DATABASE_JSON:

		#asumsi disini nomor rekening 10 digit termasuk kode bank
		unique_status = False
	
		while True:
			unique_num = help.random_with_N_digits(7)
			no_rekening = f"{KODE_BANK}{unique_num}"
			
			pathFile = f"{DB_JSON_PATH}/{no_rekening}.json"
			if not os.path.isfile(pathFile):
				unique_status = True
				break

			if DEBUG:
				print("unique_num", unique_num, "unique_status", unique_status)
		
		data_nasabah = {
					"nama"  :   daftar_reqNama,
					"nik"   :   daftar_reqNik,
					"no_hp" :   daftar_reqNo_hp,
					"nominal": 0,
					"mutasi": []
				}

		with open(pathFile, "w") as f:
			json.dump(data_nasabah, f)

		result = {
					"success"   : True,
					"remark"   : "Berhasil terdaftar!",
					"data": {
								"no_rekening" : no_rekening
							}
				}
		
		return result
