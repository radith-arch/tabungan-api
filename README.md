# Dokumentasi Repo Tabungan-Api

Requirements:
    Python 3.9

Langkah menjalankan program ini:
1.  Buka terminal cmd
2.  Jalankan command berikut:

        - git clone https://gitlab.com/radith-arch/tabungan-api.git

        - cd tabungan-api
        
        - pip install -r requirements.txt
        
        - python server.py

Terdapat 5 endpoint, diantaranya:
1.  Endpoint /daftar dengan method POST
    Contoh:
    Payload JSON:
    {
        "nama"  :   "alexander",        -> #string
        "nik"   :   "123456789",        -> #string
        "no_hp" :   "+6285810558000"    -> #string
    }

    Response JSON 200:
    {
        "success"   : true,
        "remark"    : "Berhasil terdaftar",
        "data"      :   {
                            "no_rekening" : "4514059806"
                        }
    }

    Response JSON 400:
    {
        "success"   : False,
        "remark"    : ".....",
        "data"      : null
    }

    Remark pada status 400 berisi deskripsi kesalahan terkait data yg dikirim, diantaranya:
    1. Tidak berhasil terdaftar, data tidak boleh kosong atau null!
    2. Tidak berhasil terdaftar, nama dan nik harus berbentuk string!
    3. Tidak berhasil terdaftar, nik harus berisikan angka dalam bentuk string!
    4. Tidak berhasil terdaftar, nama tidak valid!
    5. Mohon database diset sebagai JSON!

2.  Endpoint /tabung dengan method PUT
    Contoh:
    Payload JSON:
    {
        "no_rekening"  :   "4514059806", -> #string
        "nominal"      :   100           -> #integer
    }

    Response JSON 200:
    {
        "success"   : true,
        "remark"    : "Saldo berhasil ditambahkan!",
        "data"      :   {
                            "saldo" : "100"
                        }
    }

    Response JSON 400:
    {
        "success"   : False,
        "remark"    : ".....",
        "data"      : null
    }

    Remark pada status 400 berisi deskripsi kesalahan terkait data yg dikirim, diantaranya:
    1. Saldo gagal ditambahkan, data tidak boleh kosong atau null!
    2. Saldo gagal ditambahkan, no_rekening harus berbentuk string dan nominal harus berbentuk integer!
    3. Saldo gagal ditambahkan, nominal tidak boleh sama atau lebih kecil dari nol!
    4. Saldo gagal ditambahkan, no_rekening tidak valid!
    5. Nomor rekening tidak dikenali!
    6. Mohon database diset sebagai JSON!

3.  Endpoint /tarik dengan method PUT
    Contoh:
    Payload JSON:
    {
        "no_rekening"  :   "4514059806",    -> #string
        "nominal"      :   100              -> #integer
    }

    Response JSON 200:
    {
        "success"   : true,
        "remark"    : "Saldo berhasil dikurangi!",
        "data"      : {
                            "saldo" : "0"
                      }
    }

    Response JSON 400:
    {
        "success"   : False,
        "remark"    : ".....",
        "data"      : null
    }

    Remark pada status 400 berisi deskripsi kesalahan terkait data yg dikirim, diantaranya:
    1. Saldo gagal dikurang, data tidak boleh kosong atau null!
    2. Saldo gagal dikurang, no_rekening harus berbentuk string dan nominal harus berbentuk integer!
    3. Saldo gagal dikurang, nominal tidak boleh sama atau lebih kecil dari nol!
    4. Saldo gagal dikurang, no_rekening tidak valid!
    5. Nomor rekening tidak dikenali!
    6. Mohon database diset sebagai JSON!
    7. Saldo tidak cukup!

4.  Endpoint /saldo/{no_rekening} dengan method GET
    Response JSON 200:
    {
        "success"   : true,
        "remark"    : "Saldo berhasil didapatkan!",
        "data"      : {
                            "saldo" : "0"
                      }
    }

    Response JSON 400:
    {
        "success"   : False,
        "remark"    : ".....",
        "data"      : null
    }
    
    Remark pada status 400 berisi deskripsi kesalahan terkait data yg dikirim, diantaranya:
    1. Saldo gagal didapatkan, no_rekening tidak valid!
    2. Nomor rekening tidak dikenali!
    3. Mohon database diset sebagai JSON!

5.  Endpoint /mutasi/{no_rekening} dengan method POST
    Response JSON 200:
    {
        "success"   : true,
        "remark"    : "Mutasi rekening berhasil didapatkan!",
        "data"      :   {
                            "mutasi": [
                                {
                                    "waktu": "2023/03/29 00:14:30",
                                    "kode_transaksi": "C",
                                    "nominal": 100
                                },
                                {
                                    "waktu": "2023/03/29 00:15:04",
                                    "kode_transaksi": "C",
                                    "nominal": 1000
                                },
                                ...,
                                {
                                    "waktu": "2023/03/29 00:21:24",
                                    "kode_transaksi": "D",
                                    "nominal": 1000
                                }
                            ]
                        }
    }

    Response JSON 400:
    {
        "success"   : False,
        "remark"    : ".....",
        "data"      : null
    }
    
    Remark pada status 400 berisi deskripsi kesalahan terkait data yg dikirim, diantaranya:
    1. Mutasi gagal didapatkan, no_rekening tidak valid!
    2. Nomor rekening tidak dikenali!
    3. Mohon database diset sebagai JSON!