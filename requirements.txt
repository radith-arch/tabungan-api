fastapi-jwt-auth==0.5.0
python-dotenv==0.10.3
uvicorn==0.20.0
