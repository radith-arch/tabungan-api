from random import randint
import re

def random_with_N_digits(n):
    range_start = 10**(n-1)
    range_end = (10**n)-1
    return randint(range_start, range_end)

def remove_all_whitespaces(word):
    return re.sub(r'\s+', '', word)